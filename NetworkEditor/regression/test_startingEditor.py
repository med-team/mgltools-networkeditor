import sys, string
from mglutil.regression import testplus
from time import sleep

import Tkinter
from NetworkEditor.simpleNE import NetworkBuilder

withThreads = 0 # default is: multi-threading off

# allow for additional user input
if len(sys.argv):
    for myArg in sys.argv[1:]:
        if myArg[:11] == 'withThreads':
            withThreads = int(string.strip(myArg)[-1])


def pause(sleepTime=0.4):
    sleep(sleepTime)


def test_simpleNetworkNoMaster():
    ed1 = NetworkBuilder("test builder", withShell=0)
    ed1.master.update()
    ed1.configure(withThreads=withThreads)
    pause()
    ed1.master.after(1000, ed1.exit_cb )


def test_simpleNetworkMaster():
    top = Tkinter.Toplevel()
    ed2 = NetworkBuilder("test builder1", master=top, withShell=0)
    ed2.master.update()
    ed2.configure(withThreads=withThreads)
    pause()
    ed2.master.after(1000, ed2.exit_cb )


def test_startWithDifferentCanvasSize():
    top = Tkinter.Toplevel()
    ed3 = NetworkBuilder("test builder", withShell=0,
                        visibleWidth=612, visibleHeight=621, master=top)
    ed3.master.update()
    ed3.configure(withThreads=withThreads)
    pause()
    canvas =  ed3.currentNetwork.canvas
    assert canvas.cget('width') == '612'
    assert canvas.cget('height') == '621'
    ed3.master.after(1000, ed3.exit_cb )


harness = testplus.TestHarness(
    __name__,
    connect = lambda : __import__('NetworkEditor'),
    funs = testplus.testcollect( globals()),
    )

if __name__ == '__main__':
    print harness
    sys.exit( len( harness))
