import sys
from mglutil.regression import testplus
import test_startingEditor
import test_netEditing
import test_widgets
import test_portsAndWidgets
import test_scheduling
import test_macros

harness = testplus.TestHarness( __name__,
                                funs = [],
                                dependents = [
                                              test_startingEditor.harness,
                                              test_netEditing.harness,
                                              test_widgets.harness,
                                              test_portsAndWidgets.harness,
                                              test_scheduling.harness,
                                              ],
                                )

if __name__ == '__main__':
    print harness
    sys.exit( len( harness))
