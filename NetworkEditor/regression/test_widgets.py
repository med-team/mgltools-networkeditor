import sys, string
from mglutil.regression import testplus
from NetworkEditor.simpleNE import NetworkNode
from time import sleep

ed = None
withThreads = 0 # default is: multi-threading off

# allow for additional user input
if len(sys.argv):
    for myArg in sys.argv[1:]:
        if myArg[:11] == 'withThreads':
            withThreads = int(string.strip(myArg)[-1])


def startEditor():
    from NetworkEditor.simpleNE import NetworkBuilder
    global ed
    ed = NetworkBuilder("test builder1", withShell=0,
                        visibleWidth=400, visibleHeight=300)
    ed.master.update()
    ed.configure(withThreads=withThreads)


def quitEditor():
    ed.master.after(1000, ed.exit_cb )


def pause(sleepTime=0.4):
    ed.master.update()
    sleep(sleepTime)


def test_configureDial():
    # create a dial a call configure method
    from ViPEr.StandardNodes import DialNE
    node0 = DialNE(constrkw = {}, name='Dial')
    ed.currentNetwork.addNode(node0,100,100)
    widget = node0.inputPorts[0].widget
    cfg={'oneTurn': 3}
    apply( widget.configure, (), cfg)
    ed.currentNetwork.deleteNodes([node0])

    from ViPEr.StandardNodes import ThumbwheelNE
    node0 = ThumbwheelNE(constrkw = {}, name='Dial')
    ed.currentNetwork.addNode(node0,200,100)
    widget = node0.inputPorts[0].widget
    cfg={'oneTurn': 3}
    apply( widget.configure, (), cfg)
    ed.currentNetwork.deleteNodes([node0])

    
def test_setWidgetWhileHidden():
    # create an Entry node and set the value of the Entry while widget is
    # hidden
    from ViPEr.StandardNodes import EntryNE
    node0 = EntryNE()
    ed.currentNetwork.addNode(node0,100,100)
    w = node0.inputPorts[0].widget
    w.set('FOO test')
    node0.showInNodeWidget(node0.inputPorts[0].widget)
    assert w.get()=='FOO test'
    ed.currentNetwork.deleteNodes([node0])
    

def test_setWidgetWhileShown():
    # create an Entry node and set the value of the Entry while widget is
    # shown
    from ViPEr.StandardNodes import EntryNE
    node0 = EntryNE()
    ed.currentNetwork.addNode(node0,100,100)
    node0.showInNodeWidget(node0.inputPorts[0].widget)
    w = node0.inputPorts[0].widget
    w.set('FOO test')
    assert w.get()=='FOO test'
    ed.currentNetwork.deleteNodes([node0])


def test_ShowHideShowBug():
    # test that value of widget is restored properly after the widget
    # is destroyed and re-created

    from ViPEr.StandardNodes import EntryNE
    node0 = EntryNE()
    ed.currentNetwork.addNode(node0,100,100)

    node0.showInNodeWidget(node0.inputPorts[0].widget)
    node0.inputPorts[0].widget.set('Foo Test')
    node0.hideInNodeWidget(node0.inputPorts[0].widget)
    node0.showInNodeWidget(node0.inputPorts[0].widget)
    val = node0.inputPorts[0].widget.get()
    assert val=='Foo Test', 'after we hide the widget'
    ed.currentNetwork.deleteNodes([node0])


def test_hideWidgetInNodeWhileHidden():
    from ViPEr.StandardNodes import EntryNE
    node0 = EntryNE()
    ed.currentNetwork.addNode(node0,100,100)

    node0.hideInNodeWidget(node0.inputPorts[0].widget)
    node0.hideInNodeWidget(node0.inputPorts[0].widget)

    node0.showInNodeWidget(node0.inputPorts[0].widget)
    node0.showInNodeWidget(node0.inputPorts[0].widget)
    ed.currentNetwork.deleteNodes([node0])


def test_resizeNode():
    from ViPEr.StandardNodes import EntryNE
    node0 = EntryNE()
    ed.currentNetwork.addNode(node0,100,100)
    node0.rename('This is a really really really really long name')
    pause()

    node0.rename('abc')
    pause(1.0)

    node0.showInNodeWidget(node0.inputPorts[0].widget)
    pause(1.0)
    ed.currentNetwork.deleteNodes([node0])



def test_hideShowWidgetDoesNotTriggerRun():
    # node should not run when widget is hidden/shown
    # To test this, the node's compute function will increase a value which
    # would tell us then that the node has run. Which would be a bug.
    from ViPEr.StandardNodes import EntryNE
    node0 = EntryNE()
    
    ed.currentNetwork.addNode(node0,100,100)
    node0.showInNodeWidget(node0.inputPorts[0].widget)

    node0.private_incrData = 0 # used to store value
    node0.setFunction( # add some code to the node that will increase value
      "def doit(self, in1):\n"+\
      "  self.private_incrData = self.private_incrData + 1\n"+\
      "  self.outputData(out1=self.private_incrData)\n")
    
    w = node0.inputPorts[0].widget

    # setting the value of the widget should trigger the node to run
    w.set('Hello!')
    assert node0.private_incrData == 1 
    pause()

    # hiding the widget should not trigger the node to run
    node0.hideInNodeWidget(node0.inputPorts[0].widget)
    assert node0.private_incrData == 1
    pause()

    # showing the widget should not trigger the node to run
    node0.showInNodeWidget(node0.inputPorts[0].widget)
    assert node0.private_incrData == 1 
    pause()

    ed.currentNetwork.deleteNodes([node0])
    
    
harness = testplus.TestHarness( __name__,
                                connect = startEditor,
                                funs = testplus.testcollect( globals()),
                                disconnect = quitEditor
                                )

if __name__ == '__main__':
    print harness
    sys.exit( len( harness))
