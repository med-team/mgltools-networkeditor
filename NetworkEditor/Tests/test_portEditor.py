#########################################################################
#
# Date: Aug 2003  Authors: Daniel Stoffler, Michel Sanner
#
#       stoffler@scripps.edu
#       sanner@scripps.edu
#
# Copyright: Daniel Stoffler, Michel Sanner, and TSRI
#
#########################################################################


from time import sleep
import os

from NetworkEditor.net import Network
from NetworkEditor.simpleNE import NetworkNode, NetworkBuilder
from NetworkEditor.Tests.nodes import DialNode, PrintNode, PassNode
from NetworkEditor.Tests import helper

ed = None
def setUp():
    global ed
    ed = NetworkBuilder("test builder1", withShell=0,
                        visibleWidth=400, visibleHeight=300)
    ed.master.update()
    ed.configure(withThreads=0)


def tearDown():
    ed.exit_cb()
    import gc
    gc.collect()
    
##########################
## Helper methods
##########################
def pause(sleepTime=None):
    if sleepTime is None:
        from NetworkEditor.Tests import pauseLength as sleepTime
    ed.master.update()
    sleep(sleepTime)


##########################
## Tests
##########################

def test_000_openPortEditor():
    net = ed.currentNetwork
    n = helper.makeAndAddNode(net, 'Print', 'print', 100, 50)
    p = n.inputPorts[0]
    p.edit()
    from NetworkEditor.Editor import PortEditor
    assert isinstance(p.objEditor, PortEditor),\
           "Expected %s, got %s"%(PortEditor, p.objEditor.__class__)
    pause()
    p.objEditor.Dismiss()
    assert p._modified == False,"Expected False, got %s"%p._modified
    assert p.objEditor is None, "Expected None, got %s"%p.objEditor
    

def test_001_renamePortCancel():
    net = ed.currentNetwork
    n = helper.makeAndAddNode(net, 'Print', 'print', 100, 50)
    p = n.inputPorts[0]
    p.edit()
    p.objEditor.nameTk.set('New Name')
    pause()
    p.objEditor.Cancel()
    assert p._modified == False, "Expected False, got %s"%p._modified
    assert p.name == 'in1', "Expected 'in1', got '%s'"%p.name
    assert n.inputPortsDescr[0]['name'] == 'in1',\
           "Expected 'in1', got '%s'"%n.inputPortsDescr[0]['name']

    
def test_002_renamePortOK():
    net = ed.currentNetwork
    n = helper.makeAndAddNode(net, 'Print', 'print', 100, 50)
    p = n.inputPorts[0]
    p.edit()
    p.objEditor.nameTk.set('New Name')
    pause()
    p.objEditor.OK()
    assert p._modified == True, "Expected True, got %s"%p._modified
    assert p.name == 'New_Name', "Expected 'New_Name', got '%s'"%p.name
    assert n.inputPortsDescr[0]['name'] == 'New_Name',\
           "Expected 'New_Name', got '%s'"%n.inputPortsDescr[0]['name']


def test_003_renamePortApply():
    net = ed.currentNetwork
    n = helper.makeAndAddNode(net, 'Print', 'print', 100, 50)
    p = n.inputPorts[0]
    p.edit()
    p.objEditor.nameTk.set('New Name')
    pause()
    p.objEditor.Apply()
    assert p._modified == True, "Expected True, got %s"%p._modified
    assert p.name == 'New_Name',"Expected 'New_Name', got '%s'"%p.name
    assert n.inputPortsDescr[0]['name'] == 'New_Name',\
           "Expected 'New_Name', got '%s'"%n.inputPortsDescr[0]['name']


def test_004_RenameSaveAndReload():
    net = ed.currentNetwork
    n = helper.makeAndAddNode(net, 'Print', 'print', 100, 50)
    p = n.inputPorts[0]
    p.edit()
    p.objEditor.nameTk.set('New Name')
    pause()
    p.objEditor.OK()
   # save network and reaload it
    os.system("rm -f a_net.py*")
    ed.saveNetwork('a_net.py')
    ed.delete()
    ed.loadNetwork('a_net.py')
    n = ed.currentNetwork.nodes[0]
    p = n.inputPorts[0]
    assert p._modified == True, "Expected True, got %s"%p._modified
    assert p.name == 'New_Name',"Expected 'New_Name', got '%s'"%p.name
    assert n.inputPortsDescr[0]['name'] == 'New_Name',\
           "Expected 'New_Name', got '%s'"%n.inputPortsDescr[0]['name']
    #clean up temporary network file
    os.system("rm -f a_net.py*")


def test_005_setDatatype():
    net = ed.currentNetwork
    n = helper.makeAndAddNode(net, 'Print', 'print', 100, 50)
    p = n.inputPorts[0]
    p.edit()
    p.objEditor.dataType.setentry('float')
    pause()
    p.objEditor.Apply()
    assert p.datatypeObject['name'] == 'float',\
           "Expected float, got '%s'"%p.datatypeObject['name']
    assert n.inputPortsDescr[0]['datatype'] == 'float',\
           "Expected float, got '%s'"%n.inputPortsDescr[0]['datatype']


def test_006_setRequired():
    net = ed.currentNetwork
    n = helper.makeAndAddNode(net, 'Print', 'print', 100, 50)
    p = n.inputPorts[0]
    p.edit()
    required = p.required
    p.objEditor.requiredTk.invoke()
    pause()
    p.objEditor.Apply()
    assert p.required != required,\
           "Expected %s, got %s"%(not p.required, required)
    p.objEditor.requiredTk.invoke()
    p.objEditor.Cancel()
    assert p.required != required,\
           "Expected %s, got %s"%(not p.required, required)
    p.edit()
    p.objEditor.requiredTk.invoke()
    pause()
    p.objEditor.OK()
    assert p.required == required,\
           "Expected %s, got %s"%(p.required, required)


def test_007_setSingleConnection():
    net = ed.currentNetwork
    n = helper.makeAndAddNode(net, 'Print', 'print', 100, 50)
    p = n.inputPorts[0]
    p.edit()
    singleConnection = p.singleConnection
    # select 'auto'
    p.objEditor.singleConnectionTk.selectitem('auto')
    p.objEditor.Apply()
    pause()
    assert p.singleConnection == 'auto',\
           "Expected 'auto', got %s"%(p.singleConnection)
    # select False
    p.objEditor.singleConnectionTk.selectitem('False')
    p.objEditor.Apply()
    pause()
    assert p.singleConnection == False,\
           "Expected False, got %s"%(p.singleConnection)
    # select True
    p.objEditor.singleConnectionTk.selectitem('True')
    p.objEditor.Apply()
    pause()
    assert p.singleConnection == True,\
           "Expected True, got %s"%(p.singleConnection)
    # select 'auto' again, but hit cancel
    p.objEditor.singleConnectionTk.selectitem('auto')
    p.objEditor.Cancel()
    assert p.singleConnection != 'auto',\
           "Expected True, got %s"%(p.singleConnection)


def test_008_setTooltip():
    net = ed.currentNetwork
    n = helper.makeAndAddNode(net, 'Print', 'print', 100, 50)
    p = n.inputPorts[0]
    p.edit()
    p.objEditor.tt.settext('This is a test')
    pause()
    p.objEditor.Apply()
    assert 'This is a test' in p.balloonBase,\
           "p.balloonBase contains: %s"%p.balloonBase
    

def test_009_CreateUnbindBindWidget():
    # test if we can create and bind a Dial widget to a port, then unbind, and
    # finally rebind
    n = NetworkNode('Generic')
    ed.currentNetwork.addNode(n, 100, 50)
    n.edit()
    # add new input port
    n.objEditor.newInputPort()
    p = n.inputPorts[-1]
    # no widget
    assert p.widget is None, "Expected None, got %s"%p.widget
    # no widget, so no edit widget checkbutton
    assert p.editWtk is None, "Expected None, got %s"%p.editWtk
    # start port editor
    p.edit()
    # bind widget
    p.objEditor.widgetType.selectitem("NEDial")
    pause()
    p.objEditor.Apply()
    p.objEditor.Cancel()
    assert p.widget is not None, "p.widget is: %s"%p.widget
    assert p.visible == 0,"Expected 0, got %s"%p.visible
    # edit widget checkbutton should be bound
    assert p.editWtk is not None,"p.editWtk is %s"%p.editWtk
    # unbind widget
    p.edit()
    p.objEditor.widgetType.selectitem("Unbind")
    pause()
    p.objEditor.Apply()
    p.objEditor.Cancel()
    # no widget
    assert p.widget is None,"Expected None, got %s"%p.widget
    # no widget, so no edit widget checkbutton
    assert p.editWtk is None,"Expected None, got %s"%p.editWtk
    assert p.visible,"Expected 1, got %s"%p.visible
    # rebind widget
    p.edit()
    p.objEditor.widgetType.selectitem("Rebind")
    pause()
    p.objEditor.Apply()
    p.objEditor.Cancel()
    

## def test_startStopCodeEditorFromNodeEditor():
##     n = addNode()
##     n.edit()
##     n.objEditor.editFunctionToggle()
    
##     assert n.objEditor.nbEditorWindows == 1
##     assert n.objEditor.editVarTk.get()==1
##     assert n.objEditor.applyButton.cget('state')=='disabled'
##     assert n.objEditor.okButton.cget('state')=='disabled'
##     assert n.objEditor.cancelButton.cget('state')=='disabled'

##     # lift editor above other windows
##     n.objEditor.editFunctionToggle()
##     assert n.objEditor.nbEditorWindows == 1
##     assert n.objEditor.editVarTk.get()==1
##     assert n.objEditor.applyButton.cget('state')=='disabled'
##     assert n.objEditor.okButton.cget('state')=='disabled'
##     assert n.objEditor.cancelButton.cget('state')=='disabled'

##     # invoke apply
##     n.objEditor.funcEditorDialog.applyCmd()
##     assert n.objEditor.nbEditorWindows == 1
##     assert n.objEditor.editVarTk.get()==1
##     assert n.objEditor.applyButton.cget('state')=='disabled'
##     assert n.objEditor.okButton.cget('state')=='disabled'
##     assert n.objEditor.cancelButton.cget('state')=='disabled'

##     # invoke apply
##     n.objEditor.funcEditorDialog.okCmd()
##     assert n.objEditor.nbEditorWindows == 0
##     assert n.objEditor.editVarTk.get()==0
##     assert n.objEditor.applyButton.cget('state')=='normal'
##     assert n.objEditor.okButton.cget('state')=='normal'
##     assert n.objEditor.cancelButton.cget('state')=='normal'
