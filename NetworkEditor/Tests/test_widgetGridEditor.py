#########################################################################
#
# Date: Aug 2004  Author: Daniel Stoffler
#
#       stoffler@scripps.edu
#
#       The Scripps Research Institute (TSRI)
#       Molecular Graphics Lab
#       La Jolla, CA 92037, USA
#
# Copyright: Daniel Stoffler, and TSRI
#
#########################################################################

import sys, os
from time import sleep

from NetworkEditor.net import Network
from NetworkEditor.simpleNE import NetworkNode, NetworkBuilder
from NetworkEditor.Tests.nodes import DialNode

ed = None

###############################
## implement setUp and tearDown
###############################

def setUp():
    global ed
    ed = NetworkBuilder("test widget grid editor", withShell=0,
                        visibleWidth=400, visibleHeight=300)
    ed.master.update()
    ed.configure(withThreads=0)


def tearDown():
    ed.exit_cb()
    import gc
    gc.collect()

##########################
## Helper methods
##########################

def pause(sleepTime=None):
    if sleepTime is None:
        from NetworkEditor.Tests import pauseLength as sleepTime
    ed.master.update()
    sleep(sleepTime)


##########################
## Tests
##########################

def test_001_openCloseWidgetGridEditor():
    """test if we can open and close the widget grid editor"""
    net = ed.currentNetwork
    node = DialNode(name='Dial')
    net.addNode(node,100,100)
    node.edit()
    pause()
    node.objEditor.editGridButton.invoke()
    pause()
    assert node.objEditor.gridEditor is not None,\
           "Expected gridEditor instance, got None instead"
    # we should have an entry for the label and an entry for the widgetgridcfg
    gridEditor = node.objEditor.gridEditor
    assert len(gridEditor.grids) == 1,\
           "Expected 1, got %s"%len(gridEditor.grids)
    # and in there, we should have a tuple with 2 grids
    assert len(gridEditor.grids[0]) == 2,\
           "Expected 2, got %s"%len(gridEditor.grids[0])
    # hit cancel() in widgetEditor
    node.objEditor.gridEditor.Cancel_cb()
    pause()


def test_002_addLabelToDial():
    """test if we can add a label to the dial node"""
    net = ed.currentNetwork
    node = DialNode(name='Dial')
    net.addNode(node,100,100)
    widget = node.inputPorts[0].widget
    # no label by default:
    assert widget.tklabel.cget('text') == '',\
           "Expected '', got %s"%widget.tklabel.cget('text')
    node.edit()
    node.objEditor.editGridButton.invoke()
    gridEditor = node.objEditor.gridEditor
    labelGrid = gridEditor.grids[0][0]
    widgetGrid = gridEditor.grids[0][1]
    # now set the label to HELLO
    labelGrid.labelNameTk.set("HELLO")
    pause()
    gridEditor.Apply_cb()
    pause()
    assert widget.tklabel.cget('text') == 'HELLO',\
           "Expected HELLO, got %s"%widget.tklabel.cget('text')
    node.objEditor.gridEditor.Cancel_cb()


