##########################
## Helper methods
##########################

from NetworkEditor.simpleNE import NetworkNode
from NetworkEditor.Tests.nodes import DialNode, PrintNode, PassNode, \
     Operator3Node

def makeAndAddNode(net, nType='Generic', name='node', posx=100, posy=50):
    if nType == 'Generic':
        node = NetworkNode(name)
    elif nType == 'Dial':
        node = DialNode(name)
    elif nType == 'Print':
        node = PrintNode(name)
    elif nType == 'Pass':
        node = PassNode(name)
    elif nType == 'Operator3':
        node = Operator3Node(name)

    net.addNode(node, posx, posy)

    return node


def buildSmallNetwork(net):
    node1 = makeAndAddNode(net, nType='Dial', name='dial', posx=18, posy=20)
    node2 = makeAndAddNode(net, nType='Pass', name='pass', posx=183, posy=133)
    node3 = makeAndAddNode(net, nType='Print', name='print', posx=21, posy=173)
    net.connectNodes(node1, node2, 0, 0)
    net.connectNodes(node2, node3, 0, 0)
    return node1, node2, node3
