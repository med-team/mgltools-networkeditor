#########################################################################
#
# Date: Aug 2004  Author: Daniel Stoffler
#
#       stoffler@scripps.edu
#
#       The Scripps Research Institute (TSRI)
#       Molecular Graphics Lab
#       La Jolla, CA 92037, USA
#
# Copyright: Daniel Stoffler, and TSRI
#
#########################################################################

import sys, os
from time import sleep

from NetworkEditor.net import Network
from NetworkEditor.simpleNE import NetworkNode, NetworkBuilder
from NetworkEditor.Tests.nodes import DialNode

ed = None

###############################
## implement setUp and tearDown
###############################

def setUp():
    global ed
    ed = NetworkBuilder("test save and load networks", withShell=0,
                        visibleWidth=400, visibleHeight=300)
    ed.master.update()
    ed.configure(withThreads=0)


def tearDown():
    ed.exit_cb()
    import gc
    gc.collect()

##########################
## Helper methods
##########################

def pause(sleepTime=None):
    if sleepTime is None:
        from NetworkEditor.Tests import pauseLength as sleepTime
    ed.master.update()
    sleep(sleepTime)


##########################
## Tests
##########################

def test_001_openCloseWidgetEditor():
    """test if we can open and close the widget editor"""
    net = ed.currentNetwork
    node = DialNode(name='Dial')
    net.addNode(node,100,100)
    port = node.inputPorts[0]
    port.edit()
    pause()
    port.objEditor.editWidgetButton.invoke()
    pause()
    assert port.widget.objEditor is not None,\
           "Expected widgetEditor instance, got None instead"
    # hit cancel() in widgetEditor
    port.widget.objEditor.Cancel_cb()
    pause()


def test_002_setValuesThatDontRebuild():
    """Set a couple of values which should not rebuild the widget"""
    net = ed.currentNetwork
    node = DialNode(name='Dial')
    net.addNode(node,100,100)
    port = node.inputPorts[0]
    widget = port.widget
    origDescr = widget.getDescr()
    dial = widget.widget
    # check for initial values which we will change
    assert dial.continuous == 1,"Expected 1, got %s"%dial.continuous
    assert dial.increment == 0,"Expected 0, got %s"%dial.increment
    assert dial.min is None,"Expected None, got %s"%dial.min
    assert dial.max is None,"Expected None, got %s"%dial.max
    assert dial.oneTurn == 1,"Expected 1, got %s"%dial.oneTurn
    assert dial.precision == 2,"Expected 2, got %s"%dial.precision
    assert dial.showLabel == 1,"Expected 1, got %s"%dial.showLabel
    widget.edit()
    pause()
    ## 1) set continuous to 0
    widget.objEditor.widgetFormName['continuous'][0].invoke()
    pause()
    widget.objEditor.Apply_cb()
    # test if widget is still the same
    assert port.widget == widget,"Error: widget was rebuilt!"
    assert dial.continuous is None, "Expected 0, got %s"%dial.continuous
    ## 2) set increment to 25
    widget.objEditor.widgetFormName['increment'][0].setentry(25)
    pause()
    widget.objEditor.Apply_cb()
    assert port.widget == widget,"Error: widget was rebuilt!"
    assert dial.increment == 25,"Expected 25, got %s"%dial.increment
    ## 3) set min
    widget.objEditor.widgetFormName['min'][0].setentry(10)
    pause()
    widget.objEditor.Apply_cb()
    assert port.widget == widget,"Error: widget was rebuilt!"
    assert dial.min == 10,"Expected 10, got %s"%dial.min
    ## 4) set max
    widget.objEditor.widgetFormName['max'][0].setentry(130)
    pause()
    widget.objEditor.Apply_cb()
    assert port.widget == widget,"Error: widget was rebuilt!"
    assert dial.max == 130,"Expected 130, got %s"%dial.max
    ## 5) set oneTurn
    widget.objEditor.widgetFormName['oneTurn'][0].setentry(800)
    pause()
    widget.objEditor.Apply_cb()
    assert port.widget == widget,"Error: widget was rebuilt!"
    assert dial.oneTurn == 800,"Expected 800, got %s"%dial.oneTurn
    ## 6) set precision
    widget.objEditor.widgetFormName['precision'][0].setentry(5)
    pause()
    widget.objEditor.Apply_cb()
    assert port.widget == widget,"Error: widget was rebuilt!"
    assert dial.precision == 5,"Expected 5, got %s"%dial.precision
    ## 7) set showLabel
    widget.objEditor.widgetFormName['showLabel'][0].invoke()
    pause()
    widget.objEditor.Apply_cb()
    assert port.widget == widget,"Error: widget was rebuilt!"
    assert dial.showLabel == 0,"Expected 0, got %s"%dial.showLabel
   

def test_003_setValuesThatRebuild():
    """Change size which should rebuild the widget"""
    net = ed.currentNetwork
    node = DialNode(name='Dial')
    net.addNode(node,100,100)
    port = node.inputPorts[0]
    widget = port.widget
    origDescr = widget.getDescr()
    dial = widget.widget
    assert dial.size == 50, "Expected 50, got %s"%dial.size
    # set size
    widget.edit()
    widgetEditor = widget.objEditor
    pause()
    widget.objEditor.widgetFormName['size'][0].setentry(100)
    pause()
    widget.objEditor.Apply_cb()
    pause()
    assert port.widget != widget,"Error: widget was not rebuilt!"
    # get a handle to the new widget
    widget = port.widget
    dial = widget.widget
    assert dial.size == 100,"Expected 100, got %s"%dial.size
    # did we tell the widget editor that we have a new widget?
    assert widgetEditor.widget == widget,\
           "Widget Editor.widget was not updated!"
    # and rebuild again (double-rebuild bug)
    widgetEditor.widgetFormName['size'][0].setentry(25)
    pause()
    widgetEditor.Apply_cb()
    pause()
    assert port.widget != widget,"Error: widget was not rebuilt!"
    # get a handle to the new widget
    widget = port.widget
    dial = widget.widget
    assert dial.size == 25,"Expected 25, got %s"%dial.size
    # did we tell the widget editor that we have a new widget?
    assert widgetEditor.widget == widget,\
           "Widget Editor.widget was not updated!"    
    
