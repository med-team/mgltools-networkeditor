import sys
from time import sleep

import Tkinter
from NetworkEditor.simpleNE import NetworkBuilder

ed = None

def pause(sleepTime=None):
    if sleepTime is None:
        from NetworkEditor.Tests import pauseLength as sleepTime
    ed.master.update()
    sleep(sleepTime)


def test_01_simpleNetworkEditorNOMaster():
    global ed
    ed = NetworkBuilder("test builder1", withShell=0)
    ed.master.update()
    ed.configure(withThreads=0)
    pause()
    ed.exit_cb()   
    import gc
    gc.collect()


def test_02_simpleNetworkEditorWITHMaster():
    global ed
    top = Tkinter.Toplevel()
    ed = NetworkBuilder("test builder2", master=top, withShell=0)
    ed.master.update()
    ed.configure(withThreads=0)
    pause()
    ed.exit_cb()   
    import gc
    gc.collect()


def test_03_startWithDifferentCanvasSize():
    global ed
    top = Tkinter.Toplevel()
    ed = NetworkBuilder("test builder", withShell=0,
                        visibleWidth=612, visibleHeight=634, master=top)
    ed.master.update()
    ed.configure(withThreads=0)
    pause()
    canvas =  ed.currentNetwork.canvas
    assert canvas.cget('width') == '612',\
           "Expected 612, got %s"%canvas.cget('width')
    assert canvas.cget('height') == '634',\
           "Expected 634, got %s"%canvas.cget('height')
    ed.exit_cb()   
    import gc
    gc.collect()


